class ChangeTechnologies < ActiveRecord::Migration[6.0]
  def change
  	add_column :technologies, :highlight, :boolean
  end
end
