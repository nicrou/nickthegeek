class CreateProjectTags < ActiveRecord::Migration[6.0]
  def change
    create_table :project_tags do |t|
    	t.references :project
      t.references :tag
    end
  end
end
