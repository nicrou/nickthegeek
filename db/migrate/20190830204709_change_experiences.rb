class ChangeExperiences < ActiveRecord::Migration[6.0]
  def change
  	change_column :experiences, :end_date, :date, null: true
  end
end
