class CreateContents < ActiveRecord::Migration[6.0]
  def change
    create_table :contents do |t|
    	t.string   :key,             null:    false
    	t.text     :content

    	t.timestamps null: false
    end
  end
end
