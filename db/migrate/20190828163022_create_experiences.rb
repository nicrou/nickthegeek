class CreateExperiences < ActiveRecord::Migration[6.0]
  def change
    create_table :experiences do |t|
    	t.string   :title,         null:    false
    	t.string   :group,         null:    false
        t.string   :organization       
        t.string   :location      
    	t.date     :start_date,    null:    false
    	t.date     :end_date,      null:    false
    	t.text     :description
    	t.text     :credentials
    	t.boolean  :currently_working_on, default: false

    	t.timestamps null: false
    end
  end
end
