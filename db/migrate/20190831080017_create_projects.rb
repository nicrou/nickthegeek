class CreateProjects < ActiveRecord::Migration[6.0]
  def change
    create_table :projects do |t|
    	t.string   :title,         null:    false
    	t.string   :group,         null:    false
      t.string   :link 
      t.text     :notice
      t.text     :issues
    	t.text     :description
    	t.text     :content_source
    	t.date     :release_date
    	t.boolean  :currently_working_on, default: false

    	t.timestamps null: false
    end
  end
end
