class CreateTechnologies < ActiveRecord::Migration[6.0]
  def change
    create_table :technologies do |t|
    	t.string  :name,      null: false
    	t.integer :expertise, null: false

    	t.timestamps null: false
    end
  end
end
