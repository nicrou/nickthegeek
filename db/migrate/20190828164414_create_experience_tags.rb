class CreateExperienceTags < ActiveRecord::Migration[6.0]
  def change
    create_table :experience_tags do |t|
    	t.references :experience
      t.references :tag

      t.timestamps null: false
    end
  end
end
