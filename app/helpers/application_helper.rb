module ApplicationHelper
	def current_path?(test_path)
    request.path == test_path ? true : false
  end

  def previous_path
    return dashboard_path
  end

  def link_to_show (object, options = nil, html_options = nil, &block)
    link_to url_for(Array(object)), options, html_options do
      block.call
    end
  end

  def link_to_edit (object, options = nil, html_options = nil, &block)
    link_to url_for(Array(object).flatten.unshift(:edit)), options, html_options do
      block.call
    end
  end

  def link_to_destroy (object, options = nil, html_options = nil, &block)
    link_to object, { :method => :delete, data: { 'confirm-delete-swal': 'Are you sure?' }}.merge(options), html_options do
      block.call
    end
  end

  def concat_options(h1, h2)
    h1.merge(h2) do |_, v1, v2| 
      v1.concat(" #{v2}")
    end
  end

  def material_icon_tag(name, options = nil, html_options = nil)
    html_options = concat_options({:class => 'material-icons'}, html_options ||= {})
    html_options = convert_options_to_data_attributes(options, html_options)
    content_tag :i, options, html_options do
      name.to_s
    end
  end

  def material_true_or_false_icon_tag(v)
    if v.present?
      material_icon_tag :check
    else
      material_icon_tag :clear
    end
  end

  def experience_icon_tag(name, options = nil, html_options = nil)
    case name.to_sym
    when :job
      icon = 'fa-briefcase'
    when :degree
      icon = 'fa-scroll'
    when :certification
      icon = 'fa-certificate'
    when :volunteer
      icon = 'fa-hand-holding-heart'
    else
      "Error: experience has an invalid value (#{name})"
    end
    html_options = concat_options({:class => "fa #{icon}"}, html_options ||= {})
    html_options = convert_options_to_data_attributes(options, html_options)
    content_tag :i, options, html_options
  end

  def project_icon_tag(name, options = nil, html_options = nil)
    case name.to_sym
    when :sample
      icon = 'fa-flask'
    when :engine
      icon = 'fa-cube'
    when :game
      icon = 'fa-chess'
    when :mobile_app
      icon = 'fa-mobile'
    else
      "Error: experience has an invalid value (#{name})"
    end
    html_options = concat_options({:class => "fa #{icon}"}, html_options ||= {})
    html_options = convert_options_to_data_attributes(options, html_options)
    content_tag :i, options, html_options
  end
end
