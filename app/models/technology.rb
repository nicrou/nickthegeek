class Technology < ApplicationRecord
	scope :highlights, -> { where(highlight: true) }
	scope :no_highlights, -> { where(highlight: !true) }

	with_options presence: true do
		validates :name
		validates :expertise, numericality: { only_integer: true }
	end
end