class Tag < ApplicationRecord
	validates :name, presence: true, uniqueness: { case_sensitive: false }

  def self.save_multiple(names)
    saved_tags = [];
    names.split(",").reject(&:blank?).each do |name|
      tag = Tag.new({name: name})
      if tag.save
        saved_tags << tag
      end unless Tag.find_by_name(name)
    end
    return saved_tags
  end

  def <=> (other)
    name <=> other.name
  end

  def to_s
    name
  end
end