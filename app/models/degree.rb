class Degree < Experience
	default_scope { where(group: :degree) }

	before_save :set_group

	def self.header
		I18n.t(:degree, scope: [:views, :resume, :experiences])
	end

	private

  def set_group
    self.group = :degree
  end
end