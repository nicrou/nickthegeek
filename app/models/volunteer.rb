class Volunteer < Experience
	default_scope { where(group: :volunteer) }

	before_save :set_group

	def self.header
		I18n.t(:volunteer, scope: [:views, :resume, :experiences])
	end

	private

  def set_group
    self.group = :volunteer
  end
end