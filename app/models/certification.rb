class Certification < Experience
	default_scope { where(group: :certification) }

	before_save :set_group
	has_rich_text :credentials

	def self.header
		I18n.t(:certification, scope: [:views, :resume, :experiences])
	end

	private

  def set_group
    self.group = :certification
  end
end