class Project < ApplicationRecord
	scope :samples,     -> { where(group: :sample) }
	scope :engines,     -> { where(group: :engine) }
	scope :games,       -> { where(group: :game) }
	scope :mobile_apps, -> { where(group: :mobile_app) }

	has_many :project_tags
  has_many :tags, through: :project_tags

	has_rich_text :description
  has_rich_text :notice
  has_rich_text :issues
  has_rich_text :content_source

	with_options presence: true do
		validates :group
		validates :title
  end

  def save_tags(y)
    x = self.tags.present? ? self.tags.pluck(:id) : []
    # Validate presence of array y and convert its elements to integers.
    y = y.nil? ? [] : y.map { |s| s.to_i }
    # (x - y) is getting the elements in array x that are not in array y. These elements are to be destroyed.
    # (y - x) is getting the elements in array y that are not in array x. These elements are to be created if not exist see.
    begin
      ProjectTag.where(project: self, tag_id: (x-y)).destroy_all
      ((y - x) - self.tags.pluck(:id)).uniq.each do |b_id|
        ProjectTag.create!(project: self, tag: Tag.find(b_id))
      end
    rescue
      @not_valid_tags = true
    end
  end
end