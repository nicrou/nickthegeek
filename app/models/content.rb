class Content < ApplicationRecord
	has_rich_text :content

	with_options presence: true do
		validates :key
	end
end