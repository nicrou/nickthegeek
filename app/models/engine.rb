class Engine < Project
	default_scope { where(group: :engine) }

	before_save :set_group

	def self.header
		I18n.t(:engine, scope: [:views, :portfolio, :projects])
	end

	private

  def set_group
    self.group = :engine
  end
end