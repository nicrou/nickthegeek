class MobileApp < Project
	default_scope { where(group: :mobile_app) }

	before_save :set_group

	def self.header
		I18n.t(:mobile_app, scope: [:views, :portfolio, :projects])
	end

	private

  def set_group
    self.group = :mobile_app
  end
end