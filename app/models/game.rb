class Game < Project
	default_scope { where(group: :game) }

	before_save :set_group

	def self.header
		I18n.t(:game, scope: [:views, :portfolio, :projects])
	end

	private

  def set_group
    self.group = :game
  end
end