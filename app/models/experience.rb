class Experience < ApplicationRecord
	scope :jobs, -> { where(group: :job) }
	scope :degrees, -> { where(group: :degree) }
	scope :certifications, -> { where(group: :certification) }
	scope :volunteers, -> { where(group: :volunteer) }

	has_rich_text :description

	with_options presence: true do
		validates :group
		validates :title
	  validates :description
    validates :start_date
  end

  validate :time_range

  def self.groups
  	return [I18n.t(:job, scope: [:views, :resume, :experiences]),
            I18n.t(:degree, scope: [:views, :resume, :experiences]),
            I18n.t(:certification, scope: [:views, :resume, :experiences]),
            I18n.t(:volunteer, scope: [:views, :resume, :experiences])]
  end

  private

  def time_range
    if self.start_date.present? && self.end_date.present?
      errors.add(:start_date, I18n.t(:invalid_time, scope: [:errors, :messages])) if self.start_date > self.end_date
    end
  end
end