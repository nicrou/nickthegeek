class Sample < Project
	default_scope { where(group: :sample) }

	before_save :set_group

	def self.header
		I18n.t(:sample, scope: [:views, :portfolio, :projects])
	end

	private

  def set_group
    self.group = :sample
  end
end