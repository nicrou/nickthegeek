class Job < Experience
	default_scope { where(group: :job) }

	before_save :set_group

	def self.header
		I18n.t(:job, scope: [:views, :resume, :experiences])
	end

	private

  def set_group
    self.group = :job
  end
end