class DashboardController < AdminController
  def index
  	@technologies = Technology.order(expertise: :desc)
  	@tags = Tag.order(name: :asc)
  	@contents = Content.order(key: :desc)
  end
end