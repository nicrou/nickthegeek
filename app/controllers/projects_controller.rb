class ProjectsController < AdminController
  def index
  end
  
	def new
    @project = project_model.new
  end

  def create
    @project = project_model.new(project_params)  
    if @project.save
      respond_to do |format|
        flash[:success] = I18n.t(:success, resource_name: project_model.model_name.human, scope: [:flash, :actions, :create])
	      format.html { redirect_to projects_url }
	    end
    else
      render 'new'
    end
  end

  def edit
    @project = project_model.find(params[:id])
  end

  def update
    @project = project_model.find(params[:id])
    @project.assign_attributes(project_params)
    if project_save
	    respond_to do |format|
        flash[:success] = I18n.t(:success, resource_name: project_model.model_name.human, scope: [:flash, :actions, :update])
	      format.html { redirect_to projects_url }
	    end
	  else
      render 'edit'
    end
  end

  def destroy
    project = project_model.find(params[:id])
    project.destroy
    respond_to do |format|
      format.html { redirect_to projects_url }
      format.js   { render :layout => false }
    end
  end

  private

  def project_model
     @project_model = controller_name.classify.constantize
  end

  def project_params
    params.require(project_model.name.underscore.to_sym).permit(:title, :link, :notice, :issues, :description, :content_source, :release_date, :currently_working_on)
  end

  def project_save
    Project.transaction do
      @project.save_tags(params[project_model.name.underscore.to_sym][:tags])
      @project.save
    end
  end
end