class ExperiencesController < AdminController
  def index    
  end

	def new
    @experience = experience_model.new
  end

  def create
    @experience = experience_model.new(experience_params)  
    if @experience.save
      respond_to do |format|
        flash[:success] = I18n.t(:success, resource_name: experience_model.model_name.human, scope: [:flash, :actions, :create])
	      format.html { redirect_to experiences_url }
	    end
    else
      render 'new'
    end
  end

  def edit
    @experience = experience_model.find(params[:id])
  end

  def update
    @experience = experience_model.find(params[:id])
    if @experience.update(experience_params)
	    respond_to do |format|
        flash[:success] = I18n.t(:success, resource_name: experience_model.model_name.human, scope: [:flash, :actions, :update])
	      format.html { redirect_to experiences_url }
	    end
	  else
      render 'edit'
    end
  end

  def destroy
    experience = experience_model.find(params[:id])
    experience.destroy
    respond_to do |format|
      format.html { redirect_to experiences_url }
      format.js   { render :layout => false }
    end
  end

  private

  def experience_model
     @experience_model = controller_name.classify.constantize
  end

  def experience_params
    params.require(experience_model.name.underscore.to_sym).permit(:title, :organization, :location, :start_date, :end_date, :description, :credentials, :currently_working_on)
  end
end