class ContentsController < AdminController
  def index
    @contents = Content.order(key: :desc)
  end
  
	def new
    @content = Content.new
  end

  def create
    @content = Content.new(content_params)  
    if @content.save
      respond_to do |format|
      	flash[:success] = I18n.t(:success, resource_name: Content.model_name.human, scope: [:flash, :actions, :create])
	      format.html { redirect_to contents_url }
	    end
    else
      render 'new'
    end
  end

  def edit
    @content = Content.find(params[:id])
  end

  def update
    @content = Content.find(params[:id])
    if @content.update(content_params)
	    respond_to do |format|
	    	flash[:success] = I18n.t(:success, resource_name: Content.model_name.human, scope: [:flash, :actions, :update])
	      format.html { redirect_to contents_url }
	    end
	  else
      render 'edit'
    end
  end

  def destroy
    content = Content.find(params[:id])
    content.destroy
    respond_to do |format|
      format.html { redirect_to contents_url }
      format.js   { render :layout => false }
    end
  end

  private

  def content_params
    params.require(:content).permit(:key, :content)
  end
end