class TagsController < AdminController
  def index
    @tags = Tag.order(name: :asc)
  end

	def create
    if params.has_key?(:names) 
      saved_tags = Tag.save_multiple(params[:names])

      resource_name = saved_tags.count > 1 ? Tag.model_name.human.pluralize : Tag.model_name.human.singularize
      if saved_tags.any?
        flash[:success] = I18n.t(:success, resource_name: resource_name, scope: [:flash, :actions, :create])
      else 
        flash[:failure] = I18n.t(:failure, resource_name: resource_name, scope: [:flash, :actions, :create])
      end
      redirect_to tags_path
    end
	end

  def destroy
    tag = Tag.find(params[:id])

    tag.destroy

    respond_to do |format|
      format.html { redirect_to tags_path }
      format.js   { render :layout => false }
    end      
  end
end