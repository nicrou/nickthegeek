class TechnologiesController < AdminController
  def index
    @technologies = Technology.order(expertise: :desc)
  end

	def new
    @technology = Technology.new
  end

  def create
    @technology = Technology.new(technology_params)  
    if @technology.save
      respond_to do |format|
      	flash[:success] = I18n.t(:success, resource_name: Technology.model_name.human, scope: [:flash, :actions, :create])
	      format.html { redirect_to technologies_url }
	    end
    else
      render 'new'
    end
  end

  def edit
    @technology = Technology.find(params[:id])
  end

  def update
    @technology = Technology.find(params[:id])
    if @technology.update(technology_params)
	    respond_to do |format|
	    	flash[:success] = I18n.t(:success, resource_name: Technology.model_name.human, scope: [:flash, :actions, :update])
	      format.html { redirect_to technologies_url }
	    end
	  else
      render 'edit'
    end
  end

  def destroy
    technology = Technology.find(params[:id])
    technology.destroy
    respond_to do |format|
      format.html { redirect_to technologies_url }
      format.js   { render :layout => false }
    end
  end

  private

  def technology_params
    params.require(:technology).permit(:name, :expertise, :highlight)
  end
end