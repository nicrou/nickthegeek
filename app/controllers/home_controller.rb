class HomeController < ApplicationController
	layout 'application'

  def index
  	@experiences = Experience.order(start_date: :desc)
  	@technologies = Technology.order(expertise: :desc)
  	@contents = Content.order(key: :desc)
  end
end