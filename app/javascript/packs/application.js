// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

require("@rails/ujs").start()
require("turbolinks").start()
require("@rails/activestorage").start()
require("channels")

import '../stylesheets/application'
import 'materialize-css/dist/js/materialize'
import '@fortawesome/fontawesome-free/js/all'
import 'waypoints/lib/noframework.waypoints.min.js'

// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)

document.addEventListener("turbolinks:load", function() {
  // $('body').ready(function(){
  //   setTimeout(function(){
  //       $('body').addClass('loaded');
  //       $("blockquote>div").addClass('animate slide');
  //     }, 800);
  // })

  $('.collapsible').collapsible();
  $('.sidenav').sidenav();
  $('.tabs').tabs();

  let iconBlockWaypoint = new Waypoint({
    element: document.getElementById('personal_info'),
    handler: function(direction) {
      $(".icon-block>svg").addClass('animate zoom');
    },
    offset: '50%'
  })

  let skillSetWaypoint = new Waypoint({
    element: document.getElementById('skillset'),
    handler: function(direction) {
      $(this.element).find(".determinate").each(function(){
        let width = $(this).data("progress");
        $(this).width(width + '%');
      })
    },
    offset: '50%'
  })

  $("li.tab>a>svg, blockquote>p, .icon-block>svg").bind("animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd", function(){
    $(this).removeClass('animate');
    $(this).css('opacity', 1);
  });

  $(document).on('click', "li.tab" , function (e) {
    let icon = $(this).find("svg").first();
    $(icon).addClass('animate flip');
  })

  // $('select').material_select();

  //Syncronizing nav tabs and side-nav tabs. This fixes problems with transitions.

  $(document).on('click', '.sidenav.tabs>.tab>a', function (e) {
    e.preventDefault();
    $('.tabs-fixed-width>.tab>a').removeClass('active');
    let id = $(this).attr('href');
    let tab = $('.tabs-fixed-width>.tab>a[href="' + id + '"]');
    $(tab).click();
  })

  $(document).on('click', '.tabs-fixed-width>.tab>a', function (e) {
    $('.sidenav.tabs>.tab>a').removeClass('active');
    let id = $(this).attr('href');
    let tab = $('.sidenav.tabs>.tab>a[href="' + id + '"]');
    $(tab).addClass('active');
  })

  $(document).on('click', 'a.js-submit-message', function (e) {
    e.preventDefault();
    $("form#new_message").submit();
  })
})