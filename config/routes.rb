Rails.application.routes.draw do
  devise_for :users,
             class_name: 'User',
             module: :devise,
             controllers: { sessions: 'users/sessions',
                            passwords: 'users/passwords' },
             skip: [:unlocks, :omniauth_callbacks, :registrations],
             path_names: { sign_out: 'logout' }

  devise_scope :user do
    get 'authorization_failure', to: 'users/sessions#authorization_failure', as: :unauthorized
    get 'login' => 'users/sessions#new', :as => :login
    post 'login' => 'users/sessions#create', :as => :create_new_session
    get 'logout' => 'users/sessions#destroy', :as => :logout
  end

  get 'dashboard' => 'dashboard#index', as: 'dashboard'

  namespace :user do
    root :to => 'dashboard#index'
  end
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'index' => 'home#index', as: 'index'
  root 'home#index'

  resources :experiences,    only:   [:index]

  resources :jobs,           except: [:show, :index]
  resources :certifications, except: [:show, :index]
  resources :degrees,        except: [:show, :index]
  resources :volunteers,     except: [:show, :index]

  resources :projects,       only:   [:index]

  resources :samples,        except: [:show, :index]
  resources :engines,        except: [:show, :index]
  resources :games,          except: [:show, :index]
  resources :mobile_apps,    except: [:show, :index]

  resources :technologies,   except: [:show]
  resources :tags,           except: [:show]
  resources :contents,       except: [:show]
end